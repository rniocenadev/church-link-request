# Church Live/Viewing Request

## Project setup

```
npm install
```

## Configuration

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Run your tests

```
npm run test
```

### Lints and fixes files

```
npm run lint
```

### Run your end-to-end tests

```
npm run test:e2e
```

### Run your unit tests

```
npm run test:unit
```

### Config files

Inside `config/*.env.js`-files, you can configure your environment variables.

For more information, visit vuejs webpack environment variable [docs](http://vuejs-templates.github.io/webpack/env.html).

## What's included

### `npm run dev`

- `npm run dev`: first-in-class development experience.
  - Webpack + `vue-loader` for single file Vue components.
  - State preserving hot-reload
  - State preserving compilation error overlay
  - Lint-on-save with ESLint
  - Source maps

### `npm run build`

- `npm run build`: Production ready build.
  - JavaScript minified with [UglifyJS](https://github.com/mishoo/UglifyJS2).
  - HTML minified with [html-minifier](https://github.com/kangax/html-minifier).
  - CSS across all components extracted into a single file and minified with [cssnano](https://github.com/ben-eb/cssnano).
  - All static assets compiled with version hashes for efficient long-term caching, and a production `index.html` is auto-generated with proper URLs to these generated assets.

### `npm run unit`

- The testing files should be place under **test/unit/specs**.
- Make sure every test file ends in `.spec.js`

## Important Files

### main.js

This file will load your single page application and bootstrap all the plugins that are used. It will also serve as the entry point which will be loaded and compiled using webpack.

### app/index.vue

The main Vue file. This file will load the page inside the `router-view`-component. It will check if the user is authenticated and load the resources accordingly.

## Directory Structure

Inside the `src`-directory, are a couple directories that needs to be addressed:

### Components

Your components will be placed inside this directory.

### Layouts

Your layout files will be placed inside this directory.

### Mixins

The mixins you want to use with Vue will be placed inside this directory.

### Pages

The pages are placed inside this directory.

### Routes

In this directory you can specify the routes that are used by this application. VueRouter loads the routes located in this directory.

### Store

As mentioned before, Vuex is used as a single point of truth. To learn more about Vuex, visit the [documentation](http://vuex.vuejs.org)

### Test

Both the Unit Tests and the End-2-End Tests are within the `test/` folder. 