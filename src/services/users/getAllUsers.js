import Vue from 'vue'

const apiBaseUrl = 'http://13.239.135.96:8080'

export default () => {
  const url = `${apiBaseUrl}/api/users`

  return Vue.$http.get(url)
}
