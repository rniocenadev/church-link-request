export default {
  computed: {
    storeStates() {
      return this.$store.state
    },

    baseUrl() {
      return 'http://13.239.135.96:8080'
    },

    authenticated() {
      return this.storeStates.auth.authenticated
    },

    user() {
      return JSON.parse(localStorage.getItem('user'))
    },

    testData() {
      return this.storeStates.testData
    },
  },

  mounted() {
    console.log(this.storeStates)
  },
}
