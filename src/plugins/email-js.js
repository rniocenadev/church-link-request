import Vue from 'vue'
import emailJs from 'emailjs-com/dist/email.min'

Vue.emailjs = emailJs
Object.defineProperty(Vue.prototype, 'emailjs', {
  get() {
    return emailJs
  },
})
