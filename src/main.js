/* ============
 * Main File
 * ============
 *
 * Will initialize the application.
 */
import Vue from 'vue'

import '@zendeskgarden/css-forms/dist/index.css'
import '@zendeskgarden/css-buttons/dist/index.css'
import '@zendeskgarden/css-tabs/dist/index.css'

import './assets/css/hookup-form.css'

/* ============
 * Plugins
 * ============
 *
 */

import './plugins/vuex'
import './plugins/axios'
import { i18n } from './plugins/vue-i18n'
import { router } from './plugins/vue-router'
import './plugins/vuex-router-sync'
import './plugins/font-awesome'
import './plugins/email-js'

/* ============
 * Main App
 * ============
 *
 */

import app from './app'
import store from './store'

Vue.config.productionTip = false

store.dispatch('auth/check')

/* eslint-disable no-new */

new Vue({
  el: '#app', // Bind the Vue instance to the HTML.

  i18n, // The localization plugin.

  router, // The router.

  store, // The Vuex store.

  /**
   * Will render the application.
   *
   * @param {Function} h Will create an element.
   */
  render: h => h(app),
})
