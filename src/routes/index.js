/* ============
 * Routes File
 * ============
 *
 * The routes and redirects are defined in this file.
 */

export default [
  {
    path: '/',
    name: 'home.index',
    component: () => import('@/pages/home/home'),

    meta: {
      auth: true,
      title: 'Admin Dashboard',
    },
  },

  {
    path: '/login',
    name: 'login.index',
    component: () => import('@/pages/auth/user-login'),

    meta: {
      guest: true,
      title: 'Login',
    },
  },

  {
    path: '/request/new',
    name: 'request.new',
    component: () => import('@/pages/admin/hookup/request'),

    meta: {
      auth: true,
      title: 'Create New Request',
    },
  },

  {
    path: '/user/new',
    name: 'user.new',
    component: () => import('@/pages/admin/users/create-new'),

    meta: {
      auth: true,
      title: 'Create New User',
    },
  },

  {
    path: '/locale/new',
    name: 'locale.new',
    component: () => import('@/pages/admin/locales/create-new'),

    meta: {
      auth: true,
      title: 'Create New Locale',
    },
  },

  {
    path: '/*',
    name: '404',
    component: () => import('@/pages/home/home'),
    meta: {
      auth: false,
    },
  },
]
