/* ============
 * Actions for the auth module
 * ============
 *
 * The actions that are available on the
 * auth module.
 */

import Vue from 'vue'
import * as types from './mutation-types'

export const check = ({ commit }) => {
  commit(types.CHECK)
}

export const login = ({ commit }, token) => {
  commit(types.LOGIN, token)
}

export const logout = ({ commit }) => {
  commit(types.LOGOUT)
  Vue.router.push({
    name: 'home.index',
  })
}

export const getUserDetails = ({ commit }, user) => {
  commit(types.USER, user)
}

export default {
  check,
  login,
  logout,
  getUserDetails,
}
