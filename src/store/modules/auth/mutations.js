/* ============
 * Mutations for the auth module
 * ============
 *
 * The mutations that are available on the
 * account module.
 */

import Vue from 'vue'
import { CHECK, LOGIN, LOGOUT, USER, USER_TOKEN } from './mutation-types'

export default {
  [CHECK](state) {
    state.authenticated = !!localStorage.getItem('id_token')
    if (state.authenticated) {
      Vue.$http.defaults.headers.common.Authorization = `Bearer ${localStorage.getItem(
        'id_token'
      )}`
    }
  },

  [LOGIN](state, token) {
    console.log(token)
    state.authenticated = true
    localStorage.setItem('id_token', token)
    Vue.$http.defaults.headers.common.Authorization = `Bearer ${token}`

    Vue.router.push({
      name: 'home.index',
    })
  },

  [LOGOUT](state) {
    state.authenticated = false
    state.email = null
    localStorage.removeItem('id_token')
    Vue.$http.defaults.headers.common.Authorization = ''
  },

  [USER](state, user) {
    state.user = user
  },

  [USER_TOKEN](state, token) {
    state.user_token = token
  },
}
