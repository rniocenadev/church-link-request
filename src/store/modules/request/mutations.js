import {
  SELECTED_REQUEST,
  SELECTED_USER,
} from './mutation-types'

export default {
  [SELECTED_REQUEST](state, requestType) {
    state.selected.request = requestType
  },

  [SELECTED_USER](state, user) {
    state.selected.user = user
  },
}
