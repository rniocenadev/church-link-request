import * as types from './mutation-types'

export const selectedRequest = ({ commit }, request) => {
  commit(types.SELECTED_REQUEST, request)
}

export const selectedUser = ({ commit }, user) => {
  commit(types.SELECTED_USER, user)
}

export default {
  selectedRequest,
  selectedUser,
}
