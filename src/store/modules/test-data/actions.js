import * as types from './mutation-types'

export const testData = ({ commit }, data) => {
  commit(types.TEST_DATA, data)
}

export default {
  testData,
}
