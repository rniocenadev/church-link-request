import { TEST_DATA } from './mutation-types'

export default {
  [TEST_DATA](state, data) {
    state.testData = data
  },
}
