export default {
  testData: {
    users: [
      {
        id: 1,
        first_name: 'Test',
        last_name: 'User',
        locale_id: 1,
        church_id: 'ABCD1234',
        district_id: 1,
        role_id: 'admin',
        baptized_at: '2018-11-14',
        baptized_by: 'Baptizer Name',
        baptized_location: 'Melbourne, Australia',
        email: 'testuser@test.com',
        password: 'secret123',
        mobile_number: '123456789',
        is_elder: false,
        is_admin: false,
        created_at: '2018-11-14 23:00:00',
      },
    ],
    roles: [
      {
        id: 'admin',
        name: 'admin',
      },
      {
        id: 'user',
        name: 'user',
      },
    ],
    locales: [
      {
        id: 1,
        city: 'Brisbane',
        country: 'Australia',
        address: 'Brisbane, QLD',
        human_name: 'Brisbane, Australia',
      },
      {
        id: 2,
        city: 'Melbourne',
        country: 'Australia',
        address: '230 Blackshaws Road, Altona North',
        human_name: 'Melbourne, Australia',
      },
      {
        id: 3,
        city: 'Sydney',
        country: 'Australia',
        address: 'Sydney, NSW',
        human_name: 'Sydney, Australia',
      },
      {
        id: 4,
        city: 'Perth',
        country: 'Australia',
        address: 'Perth, WA',
        human_name: 'Perth, Australia',
      },
    ],
  },
}
