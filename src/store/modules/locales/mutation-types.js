export const VIEW_MEMBERS = 'VIEW_MEMBERS'
export const VIEW_ELDERS = 'VIEW_ELDERS'

export default {
  VIEW_MEMBERS,
  VIEW_ELDERS,
}
