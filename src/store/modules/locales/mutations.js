import {
  VIEW_MEMBERS,
  VIEW_ELDERS,
} from './mutation-types'

export default {
  [VIEW_MEMBERS](state, view) {
    state.view.members = view
  },

  [VIEW_ELDERS](state, view) {
    state.view.elders = view
  },
}
