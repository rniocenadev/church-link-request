import * as types from './mutation-types'

export const viewLocaleMembers = ({ commit }, view) => {
  commit(types.VIEW_MEMBERS, view)
}

export const viewLocaleElders = ({ commit }, view) => {
  commit(types.viewLocaleElders, view)
}

export default {
  viewLocaleMembers,
  viewLocaleElders,
}
