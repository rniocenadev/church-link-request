/* ============
 * Actions for the account module
 * ============
 *
 * The actions that are available on the
 * account module.
 */

/* eslint-disable arrow-body-style */

import * as types from './mutation-types'

export const find = ({ commit }, email) => {
  /*
   * Normally you would use a proxy to fetch the account:
   *
   * new Proxy()
   *  .find()
   *  .then((response) => {
   *    commit(types.FIND, Transformer.fetch(response));
   *  })
   *  .catch(() => {
   *    console.log('Request failed...');
   *  });
   */

  const users = [
    {
      id: 1,
      first_name: 'Test',
      last_name: 'User',
      locale_id: 1,
      church_id: 'ABCD1234',
      district_id: 1,
      role_id: 1,
      baptized_at: '2018-11-14',
      baptized_by: 'Baptizer Name',
      baptized_location: 'Melbourne, Australia',
      email: 'testuser@test.com',
      password: 'secret123',
      mobile_number: '123456789',
      is_elder: false,
      is_admin: false,
      created_at: '2018-11-14 23:00:00',
    },
  ]

  const user = users.filter(testUser => {
    return testUser.email === email
  })

  commit(types.FIND, user[0])
}

export default {
  find,
}
