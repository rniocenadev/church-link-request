module.exports = {
  baseUrl: '/',
  outputDir: 'dist',
  indexPath: 'index.html',
  assetsDir: 'static',
  filenameHashing: true,
  lintOnSave: true,
  runtimeCompiler: true,
  productionSourceMap: process.env.NODE_ENV === 'production',
  integrity: false,
  configureWebpack: {
    resolve: {
      alias: require('./aliases.config').webpack,
    },
  },
  css: {
    modules: false,
    extract: process.env.NODE_ENV === 'production',
    sourceMap: true,
  },
  devServer: {
    clientLogLevel: 'info',
    hot: true,
    hotOnly: true,
    open: true,
    openPage: 'login',
    overlay: {
      warnings: false,
      errors: true,
    },
    port: 8080,
    // options: errors-only, minimal, none, normal, verbose
    stats: 'verbose',
    watchOptions: {
      poll: true,
      ignored: '/node_modules/',
      aggregateTimeout: 300,
    },
  },
  pluginOptions: {
    i18n: {
      locale: 'en',
      fallbackLocale: 'ro',
      localeDir: 'locales',
      enableInSFC: false,
    },
  },
}
